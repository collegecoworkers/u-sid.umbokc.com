<?php 
$this->params['cat'] = $product->category_id;
$this->params['css'] = 'style-2';
$this->params['js'] = 'main-2';
?>

<div class="container object">

	<div id="main-container-image">

		<div class="title-item">
			<div class="title-text"><?= $product->title ?></div>
		</div>

		<div class="work">
			<figure class="white" style=" width: 60%;float: left;">
				<img src="<?= $product->getImage();?>" width="500" height="400" alt="" />
				<br><br>
				<?= $product->content ?>
			</figure>	

			<div class="wrapper-text-description">

				<div class="wrapper-download">
					<div class="icon-download"><img src="/public/img/icon-download.svg" alt="" width="24" height="16"/></div>
					<div class="text-download">Цена: <?= $product->price ?></div>
				</div>

				<div class="wrapper-desc"  style="border-bottom: 0;">
					<div class="icon-desc"><img src="/public/img/icon-desc.svg" alt="" width="24" height="24"/></div>
					<div class="text-desc">Краткое описание: <br><?= $product->desc ?></div>
				</div>

				<div style="text-align:center;margin: 0 auto;display: block;width: 150px;" >
					<?php if ($ordered): ?>
						<p>Уже куплено</p>
					<?php else: ?>
						<a href="/site/buy/?id=<?= $product->id ?>" class="btn">Купить</a>
					<?php endif ?>
				</div>
			</div>

		</div>
	</div>	
</div>
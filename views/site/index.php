<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<div class="container object">
	<div id="main-container-image">
		<section class="work">
			<?php foreach($products as $product): ?>
				<figure class="white">
					<a href="/site/view/?id=<?= $product->id ?>">
						<img src="<?= $product->getImage() ?>" alt="" />
						<dl>
							<dt><?= $product->title ?></dt>
							<dd><?= $product->desc ?></dd>	
						</dl>
					</a>
					<div id="wrapper-part-info">
						<div id="part-info"><?= $product->title ?></div>
					</div>
				</figure>
			<?php endforeach ?>
		</section>
	</div>
</div>
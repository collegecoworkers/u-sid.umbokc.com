<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<header>
    <section class="bottom">
        <div class="inner">
            <div class="block_secondary_menu">
                <nav>
                    <ul>
                        <?php foreach($categories as $category):?>
                            <li><a href="<?= Url::toRoute(['site/category/','id'=>$category->id]);?>"><?= $category->title?></a></li>
                        <?php endforeach;?>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
</header>


<!-- CONTENT BEGIN -->
<div id="content" class="sidebar_right">
    <div class="inner">

        <div class="block_general_title_1 w_margin_1">
            <h1>Проекты</h1>
        </div>
    
        <div class="">
            <div class="block_posts type_3">
                <?= $this->render('/partials/item', [
                    'projects'=>$projects
                ]);?>
            </div>
            
            <div class="separator" style="height:43px;"></div>
            
            <div class="block_pager_1">
                <?php
                    echo LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
                <div class="clearboth"></div>
            </div>
        </div>

        <div class="clearboth"></div>
    </div>
</div>
<!-- CONTENT END -->
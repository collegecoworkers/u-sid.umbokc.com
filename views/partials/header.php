<?php
use yii\helpers\Url;
use app\models\Category;
use app\models\CashAccount;

$categories = Category::find()->all();
if (!Yii::$app->user->isGuest) {
	$cash = CashAccount::findOne(['user_id' => Yii::$app->user->id]);
}
?>
	<!-- <a name="ancre"></a> -->

	<!-- CACHE -->
	<!-- <div class="cache"></div> -->

	<!-- HEADER -->

	<div id="wrapper-header">
		<div id="main-header" class="object">
			<div class="logo">
				<a href="/">Магазин</a>
			</div>
			<div class="nav">
				<?php if(Yii::$app->user->isGuest): ?>
					<a href="/auth/login">Войти</a>
				<?php else: ?>
					<a>На вашем счету: <?= $cash->points ?> пк</a>
					&nbsp;&nbsp;&nbsp;
					<a href="/account/index">Личный кабинет</a>
					&nbsp;&nbsp;&nbsp;
					<a href="/auth/logout">Выйти</a>
				<?php endif ?>
			</div>
			<div id="stripes"></div>
		</div>
	</div>

	<!-- NAVBAR -->
	<div id="wrapper-navbar">
		<div class="navbar object">
			<div id="wrapper-sorting">
				<?php $i = 0; foreach($categories as $category): ?>
				<?php 
					$active = (
						(isset($_GET['cat']) and $_GET['cat'] == $category->id)
						or (isset($this->params['cat']) and $this->params['cat'] == $category->id)
						or (!isset($this->params['cat']) and !isset($_GET['cat']) and $i == 0)
					);
				 ?>
					<div>
					<a href="<?= '/site/index?cat=' . $category->id ?>">
						<div
							class="<?= $active ? 'top-rated' : 'recent' ?> object">
							<?= $category->title ?>
						</div>
					</a>
					</div>
				<?php $i++; endforeach;?>
			</div>
		</div>
	</div>
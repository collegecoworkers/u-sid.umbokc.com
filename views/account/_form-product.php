<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Category;

?>

<div class="header">
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><a href="/account/index">Личный кабинет</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading no-collapse">Заполните форму</div>
				<div class="" style="margin: 1em;">
					<?php $form = ActiveForm::begin(); ?>

					<?= $form->field($model, 'title')->textInput() ?>
					<?= $form->field($model, 'price')->textInput() ?>
					<?= $form->field($model, 'category_id')->dropDownList(Category::getAllArr()) ?>
					<?= $form->field($model, 'desc')->textInput() ?>
					<?= $form->field($model, 'content')->textArea() ?>

					<?= $form->field($model, 'image')->fileInput() ?>

					<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success' ]) ?>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
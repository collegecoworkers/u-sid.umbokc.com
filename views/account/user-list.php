<?php 
use app\models\Order;
use app\models\CashAccount;

$this->title = 'Список пользователей';
?>
<div class="header">
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><a href="/account/index">Личный кабинет</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Имя</th>
						<th>Почта</th>
						<th>Админы</th>
						<th>Кол-во паокупок</th>
						<th>ПК</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($users as $item): ?>
					<?php
						$orders = Order::find()->where(['user_id' => $item->id])->count();
						$money = CashAccount::find()->where(['user_id' => $item->id])->one()->points;
					?>
						<tr>
							<td><?= $item->id ?></td>
							<td><?= $item->name ?></td>
							<td><?= $item->email ?></td>
							<td><?= $item->is_admin ? 'Да' : 'Нет' ?></td>
							<td><?= $orders ?></td>
							<td><?= $money ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
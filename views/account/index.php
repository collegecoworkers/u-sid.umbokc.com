<?php 
use app\models\Product;

$this->title = 'Личный кабинет';
?>
<div class="header">
	<div class="stats">
		<p class="stat">На вашем счету: <span class="label label-info"><?= $points ?></span> пластикоинов</p>
		<p class="stat">
			<a href="/account/payment" class="btn btn-primary" style="background: #5bc0de;border-color: #5bc0de;"><i class="fa fa-plus"></i> Пололнить</a>
		</p>
	</div>
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading no-collapse">Последние покупки<span class="label label-warning">10</span></div>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Продукт</th>
							<th>Дата</th>
							<th>Цена</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($orders as $item): ?>
						<?php $product = Product::findOne($item->product_id); ?>
						<tr>
							<td><?= $product->title ?></td>
							<td><?= date('m/d/h', strtotime($item->date)) ?></td>
							<td><?= $product->price ?> пк</td>
						</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading no-collapse">
					<span class="panel-icon pull-right">
					<a href="/account/index" title="Click to refresh"><i class="fa fa-refresh"></i></a>
					</span>
					Последние пополнения
				</div>
				<table class="table list">
					<tbody>
						<?php foreach ($payments as $item): ?>
							<tr>
								<td><p style="padding-top: 1em;" class="info"><?= $item->card_num ?></p></td>
								<td><p style="padding-top: 1em;">Дата: <?= date('m/d/h', strtotime($item->date)) ?></p></td>
								<td><p class="text-danger h3 pull-right" style="margin-top: 12px;">$<?= $item->sum ?></p></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
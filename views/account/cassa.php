<?php 
use app\models\Product;
use app\models\User;

$this->title = 'Касса';
?>
<div class="header">
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><a href="/account/index">Личный кабинет</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>Продукт</th>
						<th>Дата</th>
						<th>Пользователь</th>
						<th>Цена</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($orders as $item): ?>
						<?php
							$product = Product::findOne($item->product_id);
							$user = User::findOne($item->user_id);
							// dbg($user);
						?>
						<tr>
							<td><?= $product->title ?></td>
							<td><?= date('m/d/h', strtotime($item->date)) ?></td>
							<td><?= $user->name ?></td>
							<td><?= $product->price ?> пк</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

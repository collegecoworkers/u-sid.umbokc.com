<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Пополнение счета';
?>

<div class="header">
	<div class="stats">
		<p class="stat">На вашем счету: <span class="label label-info"><?= $points ?></span> пластикоинов</p>
		<p class="stat">
			<a href="/account/payment" class="btn btn-primary" style="background: #5bc0de;border-color: #5bc0de;"><i class="fa fa-plus"></i> Пололнить</a>
		</p>
	</div>
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><a href="/account/index">Личный кабинет</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading no-collapse">Заполните форму</div>
				<div class="" style="margin: 1em;">
					<?php $form = ActiveForm::begin(); ?>

					<?= $form->field($model, 'sum')->textInput() ?>
					<?= $form->field($model, 'card_num')->textInput(['placeholder' => '1234 5678 0987 6543']) ?>
					<?= $form->field($model, 'card_cvv')->textInput(['placeholder' => '123']) ?>
					<?= $form->field($model, 'card_date')->textInput(['placeholder' => '03/19']) ?>

					<?= Html::submitButton('Пополнить', ['class' => 'btn btn-success' ]) ?>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
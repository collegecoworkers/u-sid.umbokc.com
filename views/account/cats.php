<?php 
use app\models\Product;
use app\models\User;

$this->title = 'Категории';
?>
<div class="header">
	<h1 class="page-title"><?= $this->title ?></h1>
	<ul class="breadcrumb">
		<li><a href="/">Сайт</a> </li>
		<li><a href="/account/index">Личный кабинет</a> </li>
		<li><?= $this->title ?></li>
	</ul>
</div>
<div class="main-content">
	<div class="btn-toolbar list-toolbar">
		<a href="/account/cat-add" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>Название</th>
						<th>Кол-во продуктов</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($categories as $item): ?>
						<?php
							$products = Product::find()->where(['category_id' => $item->id])->count();
						?>
						<tr>
							<td><?= $item->title ?></td>
							<td><?= $products ?></td>
							<td>
								<a href="/account/cat-edit/?id=<?= $item->id ?>"><i class="fa fa-pencil"></i></a>
								<a href="/account/cat-delete/?id=<?= $item->id ?>" onclick="return confirm('Вы уверенны?')"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Добавление товара';
?>
<?= $this->render('_form-product', [
	'model' => $model,
]) ?>


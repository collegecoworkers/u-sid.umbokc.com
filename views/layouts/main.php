<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Category;

PublicAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

	<!-- Behavioral Meta Data -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="icon" type="image/png" href="/public/img/small-logo-01.png">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,900,900italic,700italic,700,500italic,400italic,500,300italic,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href='/public/<?= (isset($this->params['css'])) ? $this->params['css'] : 'style' ?>.css' rel='stylesheet' type='text/css'>
</head>
<body class="sticky_footer">
	<?php $this->beginBody() ?>

	<?= $this->render('/partials/header');?>
	<div id="wrapper-container">
		<?= $content ?>
	</div>
	<?= $this->render('/partials/footer');?>

	<!-- SCRIPT -->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="/public/js/jquery.scrollTo.min.js"></script>
	<script type="text/javascript" src="/public/js/jquery.localScroll.min.js"></script>
	<script type="text/javascript" src="/public/js/jquery-animate-css-rotate-scale.js"></script>
	<script type="text/javascript" src="/public/js/fastclick.min.js"></script>
	<script type="text/javascript" src="/public/js/jquery.animate-colors-min.js"></script>
	<script type="text/javascript" src="/public/js/jquery.animate-shadow-min.js"></script>    
	<script type="text/javascript" src="/public/<?= (isset($this->params['js'])) ? $this->params['js'] : 'js/main' ?>.js"></script>
	<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

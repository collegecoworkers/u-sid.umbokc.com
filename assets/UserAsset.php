<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UserAsset extends AssetBundle
{
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	
	public $css = [
		'http://fonts.googleapis.com/css?family=Open+Sans:400,700',
		'/account/lib/bootstrap/css/bootstrap.css',
		'/account/lib/font-awesome/css/font-awesome.css',
		'/account/stylesheets/theme.css',
		'/account/stylesheets/premium.css',
		'/account/stylesheets/main.css',
		// "public/style.css",
	];
	
	public $js = [
		// '/account/lib/jquery-1.11.1.min.js',
		'/account/lib/jQuery-Knob/js/jquery.knob.js',
		'/account/lib/bootstrap/js/bootstrap.js',
		'/account/js/main.js',
	];

	public $depends = [
		'yii\web\YiiAsset',
		// 'yii\bootstrap\BootstrapAsset',
	];
}

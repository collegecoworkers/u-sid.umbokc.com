<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use app\models\Order;
use app\models\Product;
use app\models\CashAccount;

class SiteController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action) {

		// if (Yii::$app->user->isGuest) {
		// 	return $this->redirect('/auth/login');
		// }

		return parent::beforeAction($action);
	}

	public function actionIndex($cat = null) {
		$products = Product::getAll($cat);

		return $this->render('index',[
			'products'=>$products,
		]);
	}

	public function actionView($id) {

		$product = Product::findOne($id);
		$ordered = Order::findOne(['user_id' => Yii::$app->user->id, 'product_id' => $product->id]) != null;
		return $this->render('single',[
			'product'=>$product,
			'ordered'=>$ordered,
		]);
	}

	public function actionBuy($id) {

		$product = Product::findOne($id);
		$order = new Order();
		$cash = CashAccount::findOne(['user_id' => Yii::$app->user->id]);

		if($product == null)
			return $this->goBack();

		if(Yii::$app->user->isGuest)
			return $this->redirect('/auth/login');

		$order->user_id = Yii::$app->user->id;
		$order->product_id = $product->id;
		$order->date = date('Y-m-d');

		if($cash->points >= $product->price){
			$cash->points -= $product->price;
			$order->save();
			$cash->save();
			// return $this->redirect(['site/view','id'=>$id]);
			return $this->render('success', [
				'id' => $id
			]);
		} else {
			return $this->render('no-maney');
		}
	}

}

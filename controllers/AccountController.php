<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

use app\models\Payment;
use app\models\CashAccount;
use app\models\Order;
use app\models\Category;
use app\models\Product;
use app\models\User;
use app\models\ImageUpload;
use yii\web\UploadedFile;

class AccountController extends Controller {

	public $layout = 'account';

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function beforeAction($action) {

		if (Yii::$app->user->isGuest) {
			return $this->redirect('/auth/login');
		}

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$cash = CashAccount::findOne(['user_id' => Yii::$app->user->id]);
		$orders = Order::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id' => SORT_DESC])->all();
		$payments = Payment::find()->where(['cash_account' => $cash->id])->orderBy(['id' => SORT_DESC])->all();

		return $this->render('index',[
			'points'=>$cash->points,
			'payments'=>$payments,
			'orders'=>$orders,
		]);
	}

	public function actionPayment() {
		$cash = CashAccount::findOne(['user_id' => Yii::$app->user->id]);
		$model = new Payment();

		if(Yii::$app->request->isPost){
			$model->load(Yii::$app->request->post());
			$model->cash_account = $cash->id;
			if($model->save()){
				$cash->points += $model->sum;
				$cash->save();
				return $this->redirect('/account/index');
			}
		}

		return $this->render('payment',[
			'points'=>$cash->points,
			'model'=>$model,
		]);
	}

	public function actionUserList() {

		$this->check_admin();

		$users = User::find()->all();

		return $this->render('user-list',[
			'users'=>$users,
		]);
	}

	public function actionCassa() {

		$this->check_admin();

		$orders = Order::find()->orderBy(['id' => SORT_DESC])->all();

		return $this->render('cassa',[
			'orders'=>$orders,
		]);
	}


	public function actionCats() {

		$this->check_admin();

		$categories = Category::find()->orderBy(['id' => SORT_DESC])->all();

		return $this->render('cats',[
			'categories'=>$categories,
		]);
	}

	public function actionCatAdd() {

		$this->check_admin();

		$model = new Category();

		if($model->load(Yii::$app->request->post()) and $model->save()){
			return $this->redirect('/account/cats');
		}

		return $this->render('cat-add',[
			'model'=>$model,
		]);
	}

	public function actionCatEdit($id) {

		$this->check_admin();

		$model = Category::findOne($id);

		if($model->load(Yii::$app->request->post()) and $model->save()){
			return $this->redirect('/account/cats');
		}

		return $this->render('cat-edit',[
			'model'=>$model,
		]);
	}

	public function actionCatDelete($id) {

		$this->check_admin();

		$model = Category::findOne($id);
		$model->delete();

		return $this->redirect(['/account/cats']);
	}


	public function actionProducts() {
		$this->check_admin();

		$products = Product::find()->orderBy(['id' => SORT_DESC])->all();

		return $this->render('products',[
			'products'=>$products,
		]);
	}

	public function actionProductAdd() {

		$this->check_admin();

		$model = new Product();
		$model_iu = new ImageUpload;

		if($model->load(Yii::$app->request->post())){

			$file = UploadedFile::getInstance($model,'image');
			if($file != null)
				$model->image = $model_iu->uploadFile($file, $model->image);

			if($model->save()){
				return $this->redirect('/account/products');
			}

		}

		return $this->render('product-add',[
			'model_iu'=>$model_iu,
			'model'=>$model,
		]);
	}

	public function actionProductEdit($id) {

		$this->check_admin();

		$model = Product::findOne($id);
		$model_iu = new ImageUpload;

		if($model->load(Yii::$app->request->post())){

			$file = UploadedFile::getInstance($model,'image');
			if($file != null)
				$model->image = $model_iu->uploadFile($file, $model->image);

			if($model->save()){
				return $this->redirect('/account/products');
			}

		}

		return $this->render('product-edit',[
			'model'=>$model,
		]);
	}

	public function actionProductDelete($id) {

		$this->check_admin();

		$model = Product::findOne($id);
		$model->delete();

		return $this->redirect(['/account/products']);
	}


	public function check_admin(){
		if(!User::is_admin())
			return $this->redirect('/account/index');
	}

}

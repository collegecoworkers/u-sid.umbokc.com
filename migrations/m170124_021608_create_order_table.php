<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m170124_021608_create_order_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {

		$this->createTable('order', [
			'id' => $this->primaryKey(),
			'user_id'=>$this->integer(),
			'product_id'=>$this->integer(),
			'date'=>$this->date(),
		]);

	}

	public function down() {
		$this->dropTable('order');
	}
}

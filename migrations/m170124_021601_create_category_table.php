<?php

use yii\db\Migration;

class m170124_021601_create_category_table extends Migration
{

	public function up()
	{
		$this->createTable('category', [
			'id' => $this->primaryKey(),
			'title'=>$this->string(),
			'desc'=>$this->text(),
		]);
	}

	public function down()
	{
		$this->dropTable('category');
	}
}

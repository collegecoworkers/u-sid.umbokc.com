<?php

use yii\db\Migration;

class m170124_021608_create_cash_account_table extends Migration {

	public function up() {

		$this->createTable('cash_account', [
			'id' => $this->primaryKey(),
			'user_id'=>$this->integer(),
			'points'=>$this->integer(),
		]);

	}

	public function down() {
		$this->dropTable('cash_account');
	}
}

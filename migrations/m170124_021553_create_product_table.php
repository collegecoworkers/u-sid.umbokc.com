<?php

use yii\db\Migration;

class m170124_021553_create_product_table extends Migration
{
	public function up()
	{
		$this->createTable('product', [
			'id' => $this->primaryKey(),
			'title'=>$this->string(),
			'desc'=>$this->text(),
			'image'=>$this->string(),
			'content'=>$this->text(),
			'price'=>$this->integer(),
			'category_id'=>$this->integer(),
		]);
	}

	public function down()
	{
		$this->dropTable('product');
	}
}

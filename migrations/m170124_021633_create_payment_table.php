<?php

use yii\db\Migration;

class m170124_021633_create_payment_table extends Migration
{
	public function up()
	{
		$this->createTable('payment', [
			'id' => $this->primaryKey(),
			'cash_account'=>$this->integer(),
			'date'=>$this->date(),
			'sum'=>$this->integer(),
			'card_num'=>$this->string(),
			'card_cvv'=>$this->string(),
			'card_date'=>$this->string(),
		]);
	}

	public function down()
	{
		$this->dropTable('payment');
	}
}

<?php

namespace app\models;

use Yii;

class CashAccount extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'cash_account';
	}

	public function rules()
	{
		return [
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'points' => 'монеты',
		];
	}

}
